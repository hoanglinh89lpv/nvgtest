//
//  PropertyItemView.swift
//  Vehicle
//
//  Created by Linh on 24/12/2021.
//

import UIKit

class PropertyItemView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.backgroundColor = .clear
        Bundle.main.loadNibNamed(PropertyItemView.className, owner: self, options: nil)
        contentView.frame = self.bounds
        addSubview(contentView)
    }
}


struct Property {
    
}
