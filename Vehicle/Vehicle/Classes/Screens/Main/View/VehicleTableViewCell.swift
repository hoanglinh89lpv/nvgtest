//
//  VehicleTableViewCell.swift
//  Vehicle
//
//  Created by Linh on 24/12/2021.
//

import UIKit

class VehicleTableViewCell: UITableViewCell {
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var registerDateLabel: UILabel!
    @IBOutlet weak var kmLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        thumbnailImageView.layer.cornerRadius = 16
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        thumbnailImageView.cancelRequest()
    }
    
    func configure(data: Vehicle) {
        modelLabel.text = data.typeNameFull // data.model
        makeLabel.text = data.make
        //thumbnailImageView.setImageUrl(data.imageUrl)
        thumbnailImageView.setImageUrl(data.sellerLogo ?? "")
        kmLabel.text = TextHelper.formatDistance(data.km)
        priceLabel.text = TextHelper.formatPrice(data.price)
        
        if let date = data.registerDate {
            let format = DateFormatter()
            format.dateFormat = "MMM, yyyy"
            registerDateLabel.text = format.string(from: date)
        } else {
            registerDateLabel.text = ""
        }
    }
}
