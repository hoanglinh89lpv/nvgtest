//
//  MainViewController.swift
//  MyTube
//
//  Created by Linh on 1/4/20.
//  Copyright © 2020 MnL. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    @IBOutlet weak var mainTableView: UITableView!
    
    var vehicles = [Vehicle]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getVehicles()
    }
}

private extension MainViewController {
    func getVehicles() {
        appRepository.getVehicles { [weak self] results in
            self?.vehicles = results
            self?.mainTableView.reloadData()
        } failure: { error in
            UIHelper.showError(error)
        }

    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension MainViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicles.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(ofType: VehicleTableViewCell.self, for: indexPath)
        cell.configure(data: vehicles[indexPath.row])
        return cell
    }
}
