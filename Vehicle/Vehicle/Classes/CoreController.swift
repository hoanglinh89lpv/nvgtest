//
//  CoreController.swift
//  SwiftTemplate
//
//  Created by linh on 11/20/16.
//  Copyright © 2016 linh. All rights reserved.
//

import UIKit

let appController = CoreController.shared
final class CoreController {
    static let shared = CoreController()
    
    var window: UIWindow!
    var mainNavigationController: UINavigationController!
    var mainViewController: MainViewController!
    
    private init() {}

    func launching(options: [UIApplication.LaunchOptionsKey: Any]?) {
        appDatabase.setup()
    }
    
    func start(window: UIWindow) {
        self.window = window
        gotoMainScreen()
        window.makeKeyAndVisible()
    }
    
    func finish() {
        appDatabase.saveContext()
    }
}

extension CoreController {
    @discardableResult func gotoMainScreen() -> MainViewController {
        mainViewController = gotoScreen(name: "Main", storyboard: "Main") as? MainViewController
        return mainViewController!
    }
}

extension CoreController {
    @discardableResult func gotoScreen(name: String, storyboard: String) -> UIViewController {
        if mainNavigationController == nil || mainNavigationController != window.rootViewController {
            mainNavigationController = UIHelper.createController(name: "\(name)NavigationController", boardName: storyboard) as? UINavigationController
            self.window.rootViewController = self.mainNavigationController
            return mainNavigationController.viewControllers.first!
        } else {
            mainNavigationController.dismiss(animated: false, completion: nil)
            return replaceScreen("\(name)ViewController", storyboard: storyboard, anim: true)
        }
    }
    
    @discardableResult func replaceScreen(_ screenName: String, storyboard: String, anim: Bool) -> UIViewController {
        let controller = UIHelper.createController(name: screenName, boardName: storyboard)
        
        self.mainNavigationController.setViewControllers([controller], animated: anim)
        
        return controller
    }
    
    func presentScreen(_ screen: UIViewController, anim: Bool) {
        if var topController = window.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            topController.present(screen, animated: anim, completion: nil)
        }
    }
}
