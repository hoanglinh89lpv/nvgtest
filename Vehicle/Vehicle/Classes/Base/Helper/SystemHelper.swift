//
//  SystemHelper.swift
//  RunaApp
//
//  Created by Hoang Linh on 11/1/18.
//  Copyright © 2018 Runa.life. All rights reserved.
//

import UIKit

func DLog(_ items: Any?..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG || BETA
    if let item = items[0] {
        Swift.print(item, separator:separator, terminator: terminator)
    }
    #endif
}
