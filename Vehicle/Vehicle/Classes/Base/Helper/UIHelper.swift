import UIKit

final class UIHelper {
    static func createController(name: String, boardName: String) -> UIViewController {
        var controller: UIViewController
        
        let storyboard = UIStoryboard.init(name: boardName, bundle: nil)
        controller = storyboard.instantiateViewController(withIdentifier: name)
        
        return controller
    }
    
    static func showError(_ error: StatusResponse, on controller: UIViewController? = nil, handler: (() -> Void)? = nil) {
        if !error.shouldShowMessage {
            return
        }
        
        var message = "Error.Alert.unknown"

        if error.statusCode == NSURLErrorCancelled  || error.statusCode == 11 {
            message = "Error.Alert.cancelled"
        }
        else if error.hasUserMessage() {
            message = error.userMessage
        }
        else if error.statusCode == NSURLErrorTimedOut {
            message = "Error.Alert.timeout"
        }
        
        if let handler = handler {
            UIHelper.showAlert(title: "Error", message: message, on: controller, okHandler: handler)
        } else {
            UIHelper.showAlert(title: "Error", message: message, on: controller)
        }
    }
    
    static func showAlert(title: String = "", message: String? = nil, on controller: UIViewController? = nil, okTitle: String = "OK", cancelTitle: String = "Cancel", okHandler: (() -> Void)? = nil, cancelHandler: (() -> Void)? = nil) {
        if TextHelper.isEmpty(text: title) && TextHelper.isEmpty(text: message) {
            return
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: okTitle, style: .default, handler: { (action) in
            okHandler?()
        }))
        alertController.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: { (action) in
            cancelHandler?()
        }))
        
        if controller == nil {
            appController.presentScreen(alertController, anim: true)
        } else {
            controller?.present(alertController, animated: true, completion: nil)
        }
    }
}
