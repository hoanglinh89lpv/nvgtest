//
//  TextHelper.swift
//  EnglishLearning
//
//  Created by linh on 6/11/17.
//  Copyright © 2018 Buuuk. All rights reserved.
//

import UIKit

final class TextHelper {
    static func isEmpty(text: String?) -> Bool {
        return (text == nil) || text!.isEmpty || text!.trimmingCharacters(in: .whitespaces).isEmpty
    }
    
    static func formatPrice(_ price: Double?) -> String? {
        guard let price = price else {
            return nil
        }
        
        let formatter = NumberFormatter()
        formatter.currencyCode = "USD"
        formatter.currencySymbol = "$"
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .currencyAccounting
        return formatter.string(from: NSNumber(value: price))
    }
    
    static func formatDistance(_ distance: Int?) -> String? {
        guard let distance = distance else {
            return nil
        }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        if let result = formatter.string(from: NSNumber(value: distance)) {
            return "\(result) km"
        }
        return nil
    }
}
