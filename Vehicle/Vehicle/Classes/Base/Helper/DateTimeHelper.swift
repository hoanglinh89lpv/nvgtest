//
//  DateTimeHelper.swift
//  Audi
//
//  Created by Linh on 11/9/18.
//  Copyright © 2018 Buuuk. All rights reserved.
//

import UIKit

final class DateTimeHelper {
    static let dateFormats = ["yyyy-MM-dd'T'HH:mm:ss",
                              "yyyy-MM-dd'T'HH:mm:ssZ",
                              "yyyy-MM-dd HH:mm:ss",
                              "yyyy-MM-dd"]
    
    static func datetimeFromUTCString(_ value: String) -> Date? {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        for it in dateFormats {
            formatter.dateFormat = it
            if let date = formatter.date(from: value) {
                return date
            }
        }
        return nil
    }
}
