//
//  UITableViewExtension.swift
//  Audi
//
//  Created by Linh on 12/20/18.
//  Copyright © 2018 Buuuk. All rights reserved.
//

import UIKit

extension UITableView {
    public func dequeueReusableCell<T: UITableViewCell>(ofType type: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: type.className, for: indexPath) as? T else {
            fatalError("Couldn't find UITableViewCell of class \(type.className)")
        }
        return cell
    }
}
