//
//  DictionaryExtension.swift
//  RxMVVM
//
//  Created by Linh on 17/10/2021.
//

import Foundation

extension Dictionary where Key == String {
    func toJsonString(prettyPrinted: Bool = true) -> String {
        if let stringData = try? JSONSerialization.data(withJSONObject: self, options: prettyPrinted ? .prettyPrinted : []) {
            if let string = String(data: stringData, encoding: .utf8) {
                return string
            }
        }
        return ""
    }
}
