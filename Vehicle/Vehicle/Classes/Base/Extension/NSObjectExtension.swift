//
//  NSObjectExtension.swift
//  RxMVVM
//
//  Created by Linh on 17/10/2021.
//

import Foundation

extension NSObject {
    static var className: String {
        return String(describing: self)
    }
    
    var className: String {
        return type(of: self).className
    }
}
