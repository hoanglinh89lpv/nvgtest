//
//  CachedImageView.swift
//  RunaApp
//
//  Created by Hoang Linh on 13/10/17.
//  Copyright © 2017 Runa.life. All rights reserved.
//

import UIKit
import Kingfisher


extension UIImageView {
    func setImageUrl(_ urlString: String, placeHolder: String? = nil, failImageName: String? = nil, adjustSize: Bool = true, completion: ((UIImage?) -> Void)? = nil) {
        var placeHolderImage: UIImage?
        if let holder = placeHolder {
            placeHolderImage = UIImage(named: holder)
        }
        self.image = placeHolderImage
        
        var failImage: UIImage?
        if let name = failImageName {
            failImage = UIImage(named: name)
        }

        var encodedUrl = urlString
        if let dencoded = urlString.removingPercentEncoding, urlString.compare(dencoded) == .orderedSame {
            encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? urlString
        }
        else {
            encodedUrl = urlString
        }
        if !encodedUrl.hasPrefix("http") {
            encodedUrl = "https://" + encodedUrl
        }
        
        let processor = DownsamplingImageProcessor(size: self.bounds.size)
        if let url = URL(string: encodedUrl) {
            self.kf.setImage(with: url, placeholder: placeHolderImage, options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ]) { result in
                switch result {
                case .success(let value):
                    completion?(value.image)
                case .failure(let error):
                    self.image = failImage ?? placeHolderImage
                    DLog("Job failed: \(error.localizedDescription)")
                    completion?(nil)
                }
            }
        }
    }

    func cancelRequest() {
        self.kf.cancelDownloadTask()
        self.layer.removeAllAnimations()
        self.image = nil
    }
}
