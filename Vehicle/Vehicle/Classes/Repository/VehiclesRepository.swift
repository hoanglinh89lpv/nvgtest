//
//  VehiclesRepository.swift
//  TestApp
//
//  Created by Linh on 23/12/2021.
//

import Foundation

extension Repository {
    func getVehicles(success: @escaping ([Vehicle]) -> Void, failure: @escaping StatusBlock) {
        let vehicles = VehicleDb.getAll()
        if !vehicles.isEmpty {
            success(vehicles)
        }
        
        webservice.getVehicles(success: { results in
            VehicleDb.saveAll(items: results)
            success(results)
        }, failure: failure)
    }
}
