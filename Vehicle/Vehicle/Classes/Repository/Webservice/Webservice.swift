//
//  Webservice.swift
//  RunaApp
//
//  Created by Hoang Linh on 14/9/17.
//  Copyright © 2018 Buuuk. All rights reserved.
//

import Foundation

class Webservice {
#if APPSTORE
    static let WebServiceUrl = "http://testautoscout.azurewebsites.net"
#else
    static let WebServiceUrl = "http://testautoscout.azurewebsites.net"
#endif

    static let VehicleApi = "api/vehicles"
}
