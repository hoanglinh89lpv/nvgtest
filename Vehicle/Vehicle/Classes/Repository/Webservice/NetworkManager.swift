//
//  NetworkManager.swift
//  RunaApp
//
//  Created by Hoang Linh on 15/9/17.
//  Copyright © 2018 Buuuk. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


typealias StatusBlock = (StatusResponse) -> Void
typealias ResultBlock = (Int, JSON) -> Void
let kStatusCode = "statusCode"


enum RequestMethod {
    case get, post, patch, delete
}

let appNetworking = NetworkManager.shared
final class NetworkManager {
    static let shared = NetworkManager()
    
    private let sessionManager: Session
    
    private init() {
        let configuration = URLSessionConfiguration.default
        configuration.allowsCellularAccess = true
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 120
        
        sessionManager = Session(configuration: configuration)
    }
    
    func request(type: RequestMethod, service: String, parameters: Parameters? = nil, printResponse: Bool = true, success: @escaping ResultBlock, failure: @escaping StatusBlock) {
        
        var method = HTTPMethod.get
        var encoding: ParameterEncoding = URLEncoding.queryString

        if type == .post {
            method = .post
            encoding = JSONEncoding.default
        } else if type == .patch {
            method = .patch
            encoding = JSONEncoding.default
        } else if type == .delete {
            method = .delete
        }
        
        let fullUrl = getFullUrl(service: service)
        let requestData = sessionManager.request(fullUrl, method: method, parameters: parameters, encoding: encoding)
        requestData.responseJSON { response in
            #if DEBUG
            if printResponse {
                self.printLog(for: response, cURL: requestData.cURLString())
            }
            #endif

            switch response.result {
            case .success(let value):
                let data = JSON(value)
                let code = response.response?.statusCode ?? 0
                if code < 400 {
                    success(response.response?.statusCode ?? 0, data)
                } else {
                    failure(StatusResponse.create(json: data["error"], code: code))
                }
            case .failure(let error):
                var status = StatusResponse(msg: error.localizedDescription)
                if let code = response.response?.statusCode {
                    status.statusCode = code
                }

                failure(status)
            }
        }
    }
}

extension NetworkManager {
    private func getFullUrl(service: String) -> String {
        if service.hasPrefix("http://") || service.hasPrefix("https://") {
            return service
        }
        return Webservice.WebServiceUrl + "/" + service
    }
}

extension NetworkManager {
    private func printLog<T>(for response: AFDataResponse<T>, cURL: String) {
        DispatchQueue.global().async {
            var logStrs: [String] = [ "Request with cURL: \n\(cURL)"]
            defer {
                DLog(logStrs.joined(separator: "\n"))
            }
            let statusCode = response.response?.statusCode ?? 0
            if let start = response.metrics?.taskInterval.start, let duration = response.metrics?.taskInterval.duration {
                logStrs.append("start \(start)")
                logStrs.append("duration \(String(format: "%.3f", duration))s")
            }
            logStrs.append("Response header: \((response.response?.allHeaderFields as? [String: Any])?.toJsonString(prettyPrinted: false) ?? "")")
            logStrs.append("Status code: \(statusCode)")
            var dataDebug: String!
            
            switch response.result {
            case .success(let data as Any):
                dataDebug = "data:\n"
                if let data = data as? [String: Any] {
                    dataDebug += data.toJsonString()
                }
                else if let data = data as? String {
                    dataDebug += data
                }
                else if let data = data as? Data, let str = String(data: data, encoding: .utf8) {
                    dataDebug += str
                }
                else {
                    dataDebug += "\(data)"
                }
                logStrs.append(dataDebug)
                break
            case .failure(let error):
                logStrs.append("Error: \(error)")
                break
            }
        }
    }
}

extension Request {
    public func cURLString() -> String {
        guard
            let request = lastRequest,
            let url = request.url,
            let host = url.host,
            let method = request.httpMethod else { return "$ curl command could not be created" }
        var includeHeader = false
        #if DEBUG
        includeHeader = true
        #endif
        var components = ["curl -v \"\(url.absoluteString)\""]

        components.append("-X \(method)")

        if let credentialStorage = delegate?.sessionConfiguration.urlCredentialStorage {
            let protectionSpace = URLProtectionSpace(host: host,
                                                     port: url.port ?? 0,
                                                     protocol: url.scheme,
                                                     realm: host,
                                                     authenticationMethod: NSURLAuthenticationMethodHTTPBasic)

            if let credentials = credentialStorage.credentials(for: protectionSpace)?.values {
                for credential in credentials {
                    guard let user = credential.user, let password = credential.password else { continue }
                    components.append("-u \(user):\(password)")
                }
            } else {
                if let credential = credential, let user = credential.user, let password = credential.password {
                    components.append("-u \(user):\(password)")
                }
            }
        }

        if let configuration = delegate?.sessionConfiguration, configuration.httpShouldSetCookies {
            if
                let cookieStorage = configuration.httpCookieStorage,
                let cookies = cookieStorage.cookies(for: url), !cookies.isEmpty {
                let allCookies = cookies.map { "\($0.name)=\($0.value)" }.joined(separator: ";")

                components.append("-b \"\(allCookies)\"")
            }
        }
        if includeHeader {
            var headers = HTTPHeaders()
            
            if let sessionHeaders = delegate?.sessionConfiguration.headers {
                for header in sessionHeaders where header.name != "Cookie" {
                    headers[header.name] = header.value
                }
            }
            
            for header in request.headers where header.name != "Cookie" {
                headers[header.name] = header.value
            }
            
            for header in headers {
                let escapedValue = header.value.replacingOccurrences(of: "\"", with: "\\\"")
                components.append("-H \"\(header.name): \(escapedValue)\"")
            }
        }
        if let httpBodyData = request.httpBody {
            let httpBody = String(decoding: httpBodyData, as: UTF8.self)
            var escapedBody = httpBody.replacingOccurrences(of: "\\\"", with: "\\\\\"")
            escapedBody = escapedBody.replacingOccurrences(of: "\"", with: "\\\"")

            components.append("-d \"\(escapedBody)\"")
        }

        return components.joined(separator: " \\\n\t")
    }
}
