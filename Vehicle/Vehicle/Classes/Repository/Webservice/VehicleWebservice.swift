//
//  VehicleWebservice.swift
//  TestApp
//
//  Created by Linh on 23/12/2021.
//

import Foundation

extension Webservice {
    func getVehicles(success: @escaping ([Vehicle]) -> Void, failure: @escaping StatusBlock) {
        appNetworking.request(type: .get, service: Webservice.VehicleApi, success: { (code, response) in
            if code == 200 {
                var results = [Vehicle]()
                if let array = response["results"].array {
                    for it in array {
                        results.append(Vehicle.create(json: it))
                    }
                }
                success(results)
            } else {
                failure(StatusResponse.create(json: response, code: code))
            }
        }, failure: failure)
    }
}
