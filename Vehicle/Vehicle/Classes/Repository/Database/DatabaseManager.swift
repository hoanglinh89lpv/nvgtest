//
//  DatabaseManager.swift
//  MyTube
//
//  Created by LINH HOANG on 3/9/18.
//  Copyright © 2018 MnL. All rights reserved.
//

import CoreData

let appDatabase = DatabaseManager.shared
class DatabaseManager {
    static let shared = DatabaseManager()
    
    var moc: NSManagedObjectContext!
    var persistentContainer: NSPersistentContainer!
    
    private init() {
        setup()
    }
    
    func setup() {
        persistentContainer = NSPersistentContainer(name: "app_db_v1")
        persistentContainer.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                DLog("Unresolved error \(error), \(error.userInfo)")
            }
        })
        
        moc = persistentContainer.viewContext
    }
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                DLog("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func createNewObject(entityName: String) -> NSManagedObject? {
        return NSEntityDescription.insertNewObject(forEntityName: entityName, into: moc)
    }
    
    func delete(_ object: NSManagedObject) {
        moc.delete(object)
        saveContext()
    }
    
    func clearDatabase() {
        VehicleDb.deleteAll()
    }
}
