//
//  VehicleDb.swift
//  Stock
//
//  Created by Linh on 11/6/20.
//

import Foundation
import CoreData

final class VehicleDb {
    static let request = NSFetchRequest<VehicleEntity>(entityName: "Vehicle")
    
    static func getAll() -> [Vehicle] {
        request.predicate = nil
        request.fetchLimit = 0

        do {
            let entities = try appDatabase.moc.fetch(request)
            if !entities.isEmpty {
                var results = [Vehicle]()
                for ent in entities {
                    results.append(convertToData(entity: ent))
                }

                return results
            }
        } catch {
            DLog("get all user cars failed: \(error.localizedDescription)")
        }

        return []
    }

    static func get(id: Int) -> VehicleEntity? {
        request.predicate = NSPredicate(format: "vehicleId == %d", id)
        request.fetchLimit = 1

        do {
            let entities = try appDatabase.moc.fetch(request)
            return entities.first
        } catch {
            DLog("get profile failed: \(error.localizedDescription)")
        }

        return nil
    }
    
    static func saveAll(items: [Vehicle]) {
        var ids = [Int]()
        for it in items {
            if let id = it.id {
                ids.append(id)
            }
        }

        if ids.isEmpty {
            request.predicate = nil
        } else {
            request.predicate = NSPredicate(format: "NOT (vehicleId IN %@)", ids)
        }
        request.fetchLimit = 0

        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request as! NSFetchRequest<NSFetchRequestResult>)
        do {
            try appDatabase.moc.execute(deleteRequest)
        } catch {
            DLog("clear user car failed: \(error.localizedDescription)")
        }

        for it in items {
            save(it, update: false)
        }
        appDatabase.saveContext()
    }

    static func save(_ item: Vehicle, update: Bool = true) {
        if let id = item.id, let entity = self.get(id: id) {
            self.update(entity: entity, with: item)
            if update {
                appDatabase.saveContext()
            }
        } else if let newObj = appDatabase.createNewObject(entityName: "Vehicle") as? VehicleEntity {
            self.update(entity: newObj, with: item)
            if update {
                appDatabase.saveContext()
            }
        }
    }

    static func convertToData(entity: VehicleEntity) -> Vehicle {
        var detail = Vehicle()

        detail.id = Int(entity.vehicleId)
        detail.model = entity.model
        detail.make = entity.make
        detail.km = Int(entity.km)
        detail.price = entity.price
        detail.dateCreated = entity.createdDate

        return detail
    }

    static func update(entity: VehicleEntity, with item: Vehicle) {
        entity.vehicleId = Int64(item.id ?? 0)
        entity.model = item.model
        entity.make = item.make
        entity.km = Int64(item.km ?? 0)
        entity.price = item.price ?? 0.0
        entity.createdDate = item.dateCreated
    }

    static func deleteAll() {
        request.predicate = nil
        request.fetchLimit = 0
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request as! NSFetchRequest<NSFetchRequestResult>)
        do {
            try appDatabase.moc.execute(deleteRequest)
        } catch {
            DLog("clear profile failed: \(error.localizedDescription)")
        }
        appDatabase.saveContext()
    }
}
