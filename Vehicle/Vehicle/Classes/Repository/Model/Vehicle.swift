//
//  Vehicle.swift
//  Vehicle
//
//  Created by Linh on 24/12/2021.
//

import SwiftyJSON

struct Vehicle {
    var id, km: Int?
    var price: Double?
    var model, typeNameFull, make, dateCreated: String?
    var images: VehicleImage?
    var sellerLogo: String?
    
    var imageUrl: String {
        if let host = images?.host, let path = images?.businessImageRelativePath {
            return "\(host)/\(path)"
        }
        return ""
    }
    
    var registerDate: Date? {
        if let text = dateCreated {
            return DateTimeHelper.datetimeFromUTCString(text)
        }
        
        return nil
    }
    
    static func create(json: JSON) -> Vehicle {
        var data = Vehicle()
        
        data.id = json["id"].int
        data.km = json["km"].int
        data.price = json["price"].double
        data.model = json["model"].string
        data.typeNameFull = json["typeNameFull"].string
        data.make = json["make"].string
        data.dateCreated = json["dateCreated"].string
        data.sellerLogo = json["seller"]["logoUrl"].string
        
        do {
            data.images = VehicleImage(data: try json["images"].rawData())
        } catch {
            DLog("parse images error \(error.localizedDescription)")
        }
        
        return data
    }
    
    enum CodingKeys: String, CodingKey {
        case id, model, typeNameFull, make, images, km, price, dateCreated
    }
}

struct VehicleImage: Codable {
    var host: String?
    var businessImageRelativePath: String?
    
    enum CodingKeys: String, CodingKey {
        case host, businessImageRelativePath
    }
    
    init?(data: Data) {
        do {
            let it = try JSONDecoder().decode(VehicleImage.self, from: data)
            self = it
        } catch {
            DLog(error)
            return nil
        }
    }
}

