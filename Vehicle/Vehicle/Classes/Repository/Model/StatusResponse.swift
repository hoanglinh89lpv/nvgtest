//
//  StatusDetail.swift
//  RunaApp
//
//  Created by Hoang Linh on 18/9/17.
//  Copyright © 2018 Buuuk. All rights reserved.
//

import SwiftyJSON

struct StatusResponse {
    var isSuccess = false
    var statusCode = 0
    var userMessage = ""
    var shouldShowMessage = true
    
    static func create(json: JSON, code: Int) -> StatusResponse {
        var detail = StatusResponse()
        
        detail.statusCode = code
        if let message = json["statusMessage"].string {
            detail.userMessage = message
        } else if let messages = json["statusMessage"].array {
            for msg in messages {
                if let message = msg.string {
                    detail.userMessage.append("\(message)\n")
                }
            }
        }
        
        return detail
    }
    
    init(success: Bool = false, code: Int = 0, msg: String = "") {
        isSuccess = success
        statusCode = code
        userMessage = msg
    }
    
    func hasUserMessage() -> Bool {
        return statusCode > 0 && !TextHelper.isEmpty(text: userMessage)
    }
}
