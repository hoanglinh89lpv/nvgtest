//
//  VehicleDbTest.swift
//  mfmaTests
//
//  Created by Linh on 2/27/20.
//  Copyright © 2020 Buuuk. All rights reserved.
//

import XCTest
@testable import Vehicle

class VehicleDbTest: XCTestCase {

    override func setUp() {
        super.setUp()
        appDatabase.clearDatabase()
        appDatabase.setup()
    }
    
    override func tearDown() {
        super.tearDown()
        appDatabase.clearDatabase()
    }

    func testSaveVehicle() {
        // given
        let vehicle = getTestVehicle()

        // when
        VehicleDb.save(vehicle)
        let savedVehicle = VehicleDb.get(id: vehicle.id!)
        let result = VehicleDb.convertToData(entity: savedVehicle!)

        // then
        XCTAssertEqual(result.id, vehicle.id, "Vehicle id not match!")
        XCTAssertEqual(result.model, vehicle.model, "Vehicle model not match!")
    }

    func testConvertVehicle() {
        // given
        let vehicle = getTestVehicle()

        // when
        VehicleDb.save(vehicle)
        let savedVehicle = VehicleDb.get(id: vehicle.id!)
        let result = VehicleDb.convertToData(entity: savedVehicle!)

        // then
        XCTAssertEqual(result.id, vehicle.id, "Vehicle id not match!")
        XCTAssertEqual(result.id, vehicle.id, "Vehicle username not match!")
    }
}

private extension VehicleDbTest {
    func getTestVehicle() -> Vehicle {
        var profile = Vehicle()
        profile.id = 4019102
        profile.model = "M3"
        profile.make = "BMW"
        profile.price = 18600.0
        profile.km = 189000
        
        return profile
    }
}
