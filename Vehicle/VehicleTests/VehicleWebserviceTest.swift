//
//  ProfileWebserviceTest.swift
//  TestAppTests
//
//  Created by Linh on 23/12/2021.
//

import XCTest
@testable import Vehicle

class ProfileWebserviceTest: XCTestCase {

    let webservice = Webservice()

    override func tearDown() {
        super.tearDown()
        HttpStubsHelper.clearData()
    }

    func testGetProfile() {
        // given
        HttpStubsHelper.stubData(filePath: "Response/VehiclesResponse.json", inClass: type(of: self))
        let expectation = XCTestExpectation(description: "Performs get profile request")

        // when
        appRepository.getVehicles { results in
            // then
            XCTAssertEqual(results.count, 20, "Profile id response not match!")
            XCTAssertEqual(results.first?.model, "M3", "Profile id response not match!")
            expectation.fulfill()
        } failure: { error in
            XCTAssertFalse(true, "get profile failed \(error.userMessage)")
        }

        wait(for: [expectation], timeout: 2)
    }
}
