//
//  HttpStubsHelper.swift
//  TestApp
//
//  Created by Linh on 23/12/2021.
//

import Foundation
import OHHTTPStubs

final class HttpStubsHelper {
    static func stubData(filePath: String, inClass: AnyClass, host: String = "api.example.com") {
        stub(condition: isHost(host)) { _ in
            let stubPath = OHPathForFile(filePath, inClass)
            return fixture(filePath: stubPath!, status: 200, headers: ["Content-Type":"application/json"])
        }
    }
    
    static func clearData() {
        HTTPStubs.removeAllStubs()
    }
}
